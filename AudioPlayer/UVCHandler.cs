﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Media;
using Windows.Media.Playback;
using Playlist;

namespace AudioPlayer
{
    internal class UvcHandler
    {
        private SystemMediaTransportControls controls;
        private BackgroundPlayer player;

        internal UvcHandler(BackgroundPlayer player)
        {
            this.player = player;
            Initialize();            
        }

        private void Initialize()
        {
            controls = SystemMediaTransportControls.GetForCurrentView();
            controls.IsEnabled = true;
            controls.IsNextEnabled = true;
            controls.IsPauseEnabled = true;
            controls.IsPlayEnabled = true;
            controls.IsPreviousEnabled = true;
            controls.ButtonPressed += ControlButtonPressed;
        }

        private void ControlButtonPressed(object sender, SystemMediaTransportControlsButtonPressedEventArgs e)
        {
            if (e.Button == SystemMediaTransportControlsButton.Next)
                player.PlaylistManager.MoveNext(true);
            if (e.Button == SystemMediaTransportControlsButton.Previous)
                player.PlaylistManager.MovePrevious();

            if (e.Button == SystemMediaTransportControlsButton.Play)
                BackgroundMediaPlayer.Current.Play();

            if (e.Button == SystemMediaTransportControlsButton.Pause)
                BackgroundMediaPlayer.Current.Pause();
        }

        internal void TrackChanged(Track newTrack)
        {
            controls.PlaybackStatus = MediaPlaybackStatus.Playing;
            controls.DisplayUpdater.Type = MediaPlaybackType.Music;
            controls.DisplayUpdater.MusicProperties.Title = newTrack.Name;
            controls.DisplayUpdater.MusicProperties.Artist = newTrack.Artist;

            controls.DisplayUpdater.Update();
        }
    }
}
