﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Playlist;
using Windows.ApplicationModel.Background;
using Windows.Media;
using Windows.Media.Playback;

namespace AudioPlayer
{
    public sealed class BackgroundPlayer : IBackgroundTask
    {
        private BackgroundTaskDeferral deferral;

        internal MediaPlayer MediaPlayer { get { return BackgroundMediaPlayer.Current; } }
        internal bool Playing
        {
            get
            {
                return MediaPlayer.CurrentState != MediaPlayerState.Stopped &&
                       MediaPlayer.CurrentState != MediaPlayerState.Closed;
            }
        }

        internal PlaylistManager PlaylistManager;

        internal UvcHandler UvcHandler;
        internal PlaylistHandler PlaylistHandler;

        public async void Run(IBackgroundTaskInstance taskInstance)
        {
            deferral = taskInstance.GetDeferral();

            PlaylistManager = new PlaylistManager();

            UvcHandler = new UvcHandler(this);
            PlaylistHandler = new PlaylistHandler(this);

            BackgroundMediaPlayer.MessageReceivedFromForeground += PlaylistHandler.HandleMessage;
            PlaylistManager.TrackChanged += UvcHandler.TrackChanged;

            await PlaylistManager.LoadNowPlaying();
            await PlaylistManager.LoadSettings();

            PlaylistManager.TrackChanged +=
                async track =>
                {
                    var path = PlaylistManager.CurrentTrack.Uri;
                    BackgroundMediaPlayer.Current.SetUriSource(
                        new Uri(path));
                };

            MediaPlayer.MediaEnded += (sender, args) => PlaylistManager.MoveNext();

            deferral.Complete();
        }

        public async void PlayCurrent()
        {
            if (Playing) return;

            var old = PlaylistManager.RepeatMode;
            PlaylistManager.RepeatMode = RepeatMode.Once;
            PlaylistManager.MoveNext();
            PlaylistManager.RepeatMode = old;
        }
    }
}
