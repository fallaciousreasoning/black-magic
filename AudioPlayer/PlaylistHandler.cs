﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Media.Playback;
using Playlist;

namespace AudioPlayer
{
    internal class PlaylistHandler
    {
        private BackgroundPlayer player;

        public PlaylistHandler(BackgroundPlayer player)
        {
            this.player = player;
        }

        internal async void HandleMessage(object sender, MediaPlayerDataReceivedEventArgs e)
        {
            if (e.Data.ContainsKey(Constants.UPDATE_PLAYLIST))
            {
                await player.PlaylistManager.LoadNowPlaying();
                player.PlayCurrent();
            }
            if (e.Data.ContainsKey(Constants.UPDATE_SETTINGS))
                await player.PlaylistManager.LoadSettings();
            if (e.Data.ContainsKey(Constants.SET_CURRENT_TRACK))
            {
                var trackNo = (int)e.Data[Constants.SET_CURRENT_TRACK];
                player.PlaylistManager.JumpTo(trackNo);
            }
        }
    }
}
