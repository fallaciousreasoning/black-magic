﻿using System.Threading.Tasks;
using Newtonsoft.Json;
using TorchMusic.Converters;

namespace TorchMusic
{
    [JsonConverter(typeof(TrackConverter))]
    public class TorchTrack : ITorchMusicObject
    {
        private string imageUrl;
        public const string YOUTUBE_VIDEO_URL = "http://www.youtube.com/watch?v={0}";

        public string Id { get; set; }

        public string Name { get; set; }

        public string ImageUrl
        {
            get { return ImageHelper.GetImageUrl(imageUrl); }
            set { imageUrl = value; }
        }

        public int TrackNumber { get; set; }

        public int PlayCount { get; set; }

        public int Length { get; set; }

        public string Url { get; set; }

        public string Album { get; set; }
        public string Artist { get; set; }

        public override string ToString()
        {
            return string.Format("[Track Name: {0}]", Name);
        }
    }
}
