﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace TorchMusic.Converters
{
    public abstract class TorchObjectConverter<T> : CustomCreationConverter<T> where T : class, ITorchMusicObject
    {
        protected Dictionary<string, string> Mappings = new Dictionary<string, string>()
        {
            {"image_url", "ImageUrl"},
            {"image", "ImageUrl"},
        };

        public override T Create(Type objectType)
        {
            return Activator.CreateInstance(objectType) as T;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var torchObject = Create(objectType);
            var properties = objectType.GetRuntimeProperties();

            var jObject = JObject.Load(reader);

            foreach (var jProp in jObject.Properties())
            {
                var property = properties.FirstOrDefault(p => Matches(p, jProp));
                if (property == null) continue;

                property.SetValue(torchObject, jProp.Value.ToObject(property.PropertyType));
            }

            return torchObject;
        }

        private bool Matches(PropertyInfo property, JProperty jProperty)
        {
            var pName = property.Name;
            var jName = jProperty.Name;

            return pName.ToLower() == jName.ToLower().Replace("_", "") ||
                (Mappings.ContainsKey(jName) && Mappings[jName] == pName);
        }
    }
}
