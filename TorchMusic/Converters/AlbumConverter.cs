﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Converters;

namespace TorchMusic.Converters
{
    public class AlbumConverter : TorchObjectConverter<TorchAlbum>
    {
        public AlbumConverter()
        {
            Mappings.Add("album_id", "Id");
            Mappings.Add("track", "Tracks");
            Mappings.Add("cover", "ImageUrl");
        }
    }
}
