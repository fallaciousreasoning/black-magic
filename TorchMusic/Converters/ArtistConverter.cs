﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace TorchMusic.Converters
{
    public class ArtistConverter : TorchObjectConverter<TorchArtist>
    {
        public ArtistConverter()
        {
            Mappings.Add("artist_id", "Id");
            Mappings.Add("album", "Albums");
        }
    }
}
