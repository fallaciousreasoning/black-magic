﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Converters;

namespace TorchMusic.Converters
{
    public class TrackConverter : TorchObjectConverter<TorchTrack>
    {
        public TrackConverter()
        {
            Mappings.Add("track_id", "Id");
            Mappings.Add("track", "Tracks");
            Mappings.Add("number", "TrackNumber");
        }
    }
}
