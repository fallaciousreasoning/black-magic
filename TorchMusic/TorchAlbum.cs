﻿using System.Collections.Generic;
using Newtonsoft.Json;
using TorchMusic.Converters;

namespace TorchMusic
{
    [JsonConverter(typeof(AlbumConverter))]
    public class TorchAlbum : ITorchMusicObject
    {
        public string ReleaseDate { get; set; }

        public List<TorchTrack> Tracks { get; set; }

        public string Id { get; set; }

        public string Name { get; set; }

        public string ImageUrl { get; set; }

        public string Artist { get; set; }

        public TorchAlbum()
        {
            Tracks = new List<TorchTrack>();
        }

        public override string ToString()
        {
            return string.Format("[Album Name: {0}]", Name);
        }
    }
}
