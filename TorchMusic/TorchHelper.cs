﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TorchMusic
{
    [Flags]
    public enum SearchFor
    {
        Artists = 1,
        Albums = 2,
        Tracks = 4,
        All = 8,
    }

    public class TorchHelper
    {
        #region Constants

        /// <summary>
        /// Search url for Torch Music. First argument is the query, second is the number of results of each kind (artists, albums, tracks)
        /// </summary>
        public const string SEARCH_URL =
            "http://music.torchbrowser.com/loudlee_db/server.php?cmd=music_search&q={0}&limit={1}";

        public const string SEARCH_CUSTOM_URL = "http://music.torchbrowser.com/loudlee_db/server.php?cmd=music_search&q={0}&types={2}&limit={1}";
        /// <summary>
        /// Auto complete url for Torch music. First Argument is the query, second is the number of results
        /// </summary>
        public const string AUTO_COMPLETE_URL =
            "http://music.torchbrowser.com/loudlee_db/server.php?cmd=music_auto_complete&q={0}&limit={1}";

        /// <summary>
        /// The url for getting the youtube url for a track. First argument is the track id
        /// </summary>
        public const string YOUTUBE_URL_FETCHER =
            "http://music.torchbrowser.com/loudlee_db/server.php?cmd=music_get_youtube_id&track_id={0}";

        /// <summary>
        /// Url for fetching an artist. First argument is the artist id
        /// </summary>
        public const string ARTIST_URL =
            "http://music.torchbrowser.com/loudlee_db/server.php?cmd=music_get_artist&artist_id={0}";

        /// <summary>
        /// Url for fetching an album. First argument is the album id
        /// </summary>
        public const string ALBUM_URL =
            "http://music.torchbrowser.com/loudlee_db/server.php?cmd=music_get_album&album_id={0}";

        /// <summary>
        /// Url for fetching a track. First argument is the track id
        /// </summary>
        public const string TRACK_URL =
            "http://music.torchbrowser.com/loudlee_db/server.php?cmd=music_get_track&track_id={0}";

        #endregion

        #region Search
        /// <summary>
        /// Searches torch music
        /// </summary>
        /// <param name="query">The query, the thing we're searching for</param>
        /// <param name="limit">The number of results (of each type, so say limit=4 we might get four artists, four albums and four tracks)</param>
        /// <returns>The search result</returns>
        public async Task<TorchSearchResults> SearchAsync(string query, int limit = 4)
        {
            if (string.IsNullOrEmpty(query)) return new TorchSearchResults();

            var url = string.Format(SEARCH_URL, query, limit);
            var request = WebRequest.CreateHttp(url);
            var response = await request.GetResponseAsync();

            var responseStream = response.GetResponseStream();
            TextReader reader = new StreamReader(responseStream);
            var json = await reader.ReadToEndAsync();

            var results = await Task.Factory.StartNew(() =>
            {
                dynamic d = JsonConvert.DeserializeObject(json);

                var artists = JsonConvert.DeserializeObject<List<TorchArtist>>(d.data.results.artists.results.ToString());
                var albums = JsonConvert.DeserializeObject<List<TorchAlbum>>(d.data.results.albums.results.ToString());
                var tracks = JsonConvert.DeserializeObject<List<TorchTrack>>(d.data.results.tracks.results.ToString());
                return new TorchSearchResults() { Albums = albums, Artists = artists, Tracks = tracks, Query = query};
            });
            return results;
        }

        public async Task<List<ITorchMusicObject>> Search(string query, int count = 4, SearchFor searchFor=SearchFor.All)
        {
            if (string.IsNullOrEmpty(query)) return new List<ITorchMusicObject>();

            var types = "";
            if (searchFor.HasFlag(SearchFor.Artists))
                types = AppendArgument(types, "artists");
            if (searchFor.HasFlag(SearchFor.Albums))
                types = AppendArgument(types, "albums");
            if (searchFor.HasFlag(SearchFor.Tracks))
                types = AppendArgument(types, "tracks");

            var url = string.Format((searchFor == SearchFor.All ? SEARCH_URL : SEARCH_CUSTOM_URL), query, count, types);
            var request = WebRequest.CreateHttp(url);
            var response = await request.GetResponseAsync();

            var responseStream = response.GetResponseStream();
            TextReader reader = new StreamReader(responseStream);
            var json = await reader.ReadToEndAsync();

            var results = await Task.Run(() =>
            {
                dynamic d = JsonConvert.DeserializeObject(json);
                
                var r = new List<ITorchMusicObject>();
                if (d.data.results.artists != null)
                    r.AddRange(JsonConvert.DeserializeObject<List<TorchArtist>>(d.data.results.artists.results.ToString()));
                if (d.data.results.albums != null)
                r.AddRange(JsonConvert.DeserializeObject<List<TorchAlbum>>(d.data.results.albums.results.ToString()));
                if (d.data.results.tracks != null)
                    r.AddRange(JsonConvert.DeserializeObject<List<TorchTrack>>(d.data.results.tracks.results.ToString()));
                
                return r;
            });

            return results;
        }

        private string AppendArgument(string argString, string arg)
        {
            return string.IsNullOrEmpty(argString) ? arg : string.Format("{0},{1}", argString, arg);
        }

        /// <summary>
        /// Gets auto complete results
        /// </summary>
        /// <param name="query">The query (whatever we're typing)</param>
        /// <param name="limit">The number of results we want</param>
        /// <returns>A list of potential results</returns>
        public async Task<IEnumerable<TorchAutoCompleteResult>> AutoCompleteAsync(string query, int limit = 4)
        {
            var url = string.Format(AUTO_COMPLETE_URL, query, limit);
            var request = WebRequest.CreateHttp(url);
            var response = await request.GetResponseAsync();

            var responseStream = response.GetResponseStream();
            TextReader reader = new StreamReader(responseStream);
            var json = await reader.ReadToEndAsync();

            var results =
                await Task.Factory.StartNew(() =>
                {
                    dynamic d = JsonConvert.DeserializeObject(json);
                    return JsonConvert.DeserializeObject<List<TorchAutoCompleteResult>>(d.data.results.ToString());
                });
            return results;
        }

        #endregion

        #region Artists and Albums

        /// <summary>
        /// Gets a torch artist
        /// </summary>
        /// <param name="artistId">The id of the artist</param>
        /// <returns>The artist</returns>
        public async Task<TorchArtist> GetArtistAsync(string artistId)
        {
            var url = string.Format(ARTIST_URL, artistId);
            var request = WebRequest.CreateHttp(url);
            var response = await request.GetResponseAsync();

            var responseStream = response.GetResponseStream();
            TextReader reader = new StreamReader(responseStream);
            var json = await reader.ReadToEndAsync();

            var artist = await Task.Factory.StartNew(() =>
            {
                dynamic artistJson = JsonConvert.DeserializeObject(json);
                return JsonConvert.DeserializeObject<TorchArtist>(artistJson.data.ToString()) as TorchArtist;
            });

            foreach (var album in artist.Albums)
                album.Artist = artist.Name;
            
            return artist;
        }

        /// <summary>
        /// Gets an album from Torch
        /// </summary>
        /// <param name="albumId">The id of the album</param>
        /// <returns>The album</returns>
        public async Task<TorchAlbum> GetAlbumAsync(string albumId)
        {
            var url = string.Format(ALBUM_URL, albumId);
            var request = WebRequest.CreateHttp(url);
            var response = await request.GetResponseAsync();

            var responseStream = response.GetResponseStream();
            TextReader reader = new StreamReader(responseStream);
            var json = await reader.ReadToEndAsync();

            var album = await Task.Factory.StartNew(() =>
            {
                dynamic albumJson = JsonConvert.DeserializeObject(json);
                var albums = JsonConvert.DeserializeObject<List<TorchAlbum>>(albumJson.data.album.ToString()) as List<TorchAlbum>;
                albums[0].Artist = albumJson.data.name;
                return albums[0] as TorchAlbum;
            });

            foreach (var track in album.Tracks)
            {
                track.Artist = album.Artist;
                track.Album = album.Name;
            }

            return album;
        }

        #endregion

        #region Tracks
        /// <summary>
        /// Gets a youtube url for the track
        /// </summary>
        /// <param name="track">The track</param>
        /// <returns>A youtube url, pointing to a video of the current track</returns>
        public async Task<string> GetYoutubeUrl(TorchTrack track)
        {
            return await GetYoutubeUrl(track.Id);
        }

        /// <summary>
        /// Gets a youtube url for the track
        /// </summary>
        /// <param name="trackId">The id of the track</param>
        /// <returns>A youtube url, pointing to a video of the current track</returns>
        public async Task<string> GetYoutubeUrl(string trackId)
        {
            var url = string.Format(YOUTUBE_URL_FETCHER, trackId);

            var request = WebRequest.CreateHttp(url);
            var response = await request.GetResponseAsync();
            TextReader reader = new StreamReader(response.GetResponseStream());

            dynamic d = JsonConvert.DeserializeObject(await reader.ReadToEndAsync());
            return d.data;

            return await reader.ReadToEndAsync();
        }

        public async Task<TorchTrack> GetTrack(string id)
        {
            var url = string.Format(TRACK_URL, id);

            var request = WebRequest.CreateHttp(url);
            var response = await request.GetResponseAsync();

            TextReader reader = new StreamReader(response.GetResponseStream());
            var json = await reader.ReadToEndAsync();

            var track = await Task.Factory.StartNew(() =>
            {
                dynamic trackJson = JsonConvert.DeserializeObject(json);
                var albums = JsonConvert.DeserializeObject<List<TorchAlbum>>(trackJson.data.album.ToString()) as List<TorchAlbum>;
                var album = albums[0];
                var t = album.Tracks[0];
                t.Album = album.Name;
                t.Artist = trackJson.data.name;
                return album.Tracks[0];
            });
            return track;
        }
        #endregion
    }
}
