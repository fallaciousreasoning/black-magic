﻿using System.Collections.Generic;
using Newtonsoft.Json;
using TorchMusic.Converters;

namespace TorchMusic
{
    [JsonConverter(typeof(ArtistConverter))]
    public class TorchArtist : ITorchMusicObject
    {
        public int PlayCount { get; set; }

        public int AlbumCount { get; set; }

        public int SimilarCount { get; set; }

        public bool InLibrary { get; set; }

        public List<TorchAlbum> Albums { get; set; }

        public string Id { get; set; }

        public string Name { get; set; }

        public string ImageUrl { get; set; }

        public TorchArtist()
        {
            Albums = new List<TorchAlbum>();
        }

        public override string ToString()
        {
            return string.Format("[Artist Name: {0}]", Name);
        }
    }
}
