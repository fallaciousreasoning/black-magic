﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace TorchMusic
{
    public class TorchSearchResults
    {
        public string Query { get; set; }

        [JsonProperty("artists")]
        public IEnumerable<TorchArtist> Artists { get; set; }
       
        [JsonProperty("albums")]
        public IEnumerable<TorchAlbum> Albums { get; set; }
        
        [JsonProperty("tracks")]
        public IEnumerable<TorchTrack> Tracks { get; set; }

        public TorchSearchResults()
        {
            Artists = new List<TorchArtist>();
            Albums = new List<TorchAlbum>();
            Tracks = new List<TorchTrack>();
        }
    }
}
