﻿using Newtonsoft.Json;

namespace TorchMusic
{
    public class TorchAutoCompleteResult : ITorchMusicObject
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("image")]
        public string ImageUrl { get; set; }

        public override string ToString()
        {
            return string.Format("[AutoCompleteResult Type: {0} Name: {1}]", Type, Name);
        }
    }
}
