﻿using Newtonsoft.Json;

namespace TorchMusic
{
    public interface ITorchMusicObject
    {
        string Id { get; set; }

        string Name { get; set; }

        string ImageUrl { get; set; }
    }
}
