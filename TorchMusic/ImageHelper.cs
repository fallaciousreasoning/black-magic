﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TorchMusic
{
    public enum ImageSize
    {
        Tiny,
        Small,
        Medium,
        Large,
        Huge
    }

    public static class ImageHelper
    {
        private const string IMAGE_URL =
            "http://cps-static.rovicorp.com/3/JPG_{0}/MI{1}/{2}/MI{3}.jpg?partner=allrovi.com";

        private static readonly Dictionary<ImageSize, string> sizeCodes = new Dictionary<ImageSize, string>()
        {
            {ImageSize.Tiny, "75"},
            {ImageSize.Small, "250"},
            {ImageSize.Medium, "400"},
            {ImageSize.Large, "500"},
            {ImageSize.Huge, "1080"}
        };

        public static string GetImageUrl(string serverTag, ImageSize size=ImageSize.Medium)
        {
            if (string.IsNullOrEmpty(serverTag)) return null;

            var image = Pad(serverTag.Split('|')[1], 10);
            var imageSize = GetSizeCode(size);

            return string.Format(IMAGE_URL, imageSize, image.Substring(0, 4), image.Substring(4, 3), image);
        }

        private static string Pad(string toPad, int paddedLength)
        {
            if (toPad.Length > paddedLength) throw new ArgumentException("'paddedLength must be less than that of the string to pad!'", "paddedLength");

            while (toPad.Length < paddedLength)
                toPad = toPad.Insert(0, "0");

            return toPad;
        }

        private static string GetSizeCode(ImageSize size)
        {
            return sizeCodes[size];
        }
    }
}
