﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MusicBase.MusicSources;
using MusicBase.Torch;
using SQLite;

namespace MusicBase
{
    /// <summary>
    /// Provides methods for searching, getting information on 
    /// Artists, Albums and Tracks and maintains a database of
    /// local tracks.
    /// </summary>
    public class MusicManager
    {
        private readonly TorchMusicSource torchSource;
        private readonly DatabaseMusicSource localSource;

        /// <summary>
        /// Creates a new MusicManager
        /// </summary>
        public MusicManager()
        {
            torchSource = new TorchMusicSource();
            localSource = new DatabaseMusicSource();
        }

        /// <summary>
        /// Searches local and online sources for the specified term.
        /// </summary>
        /// <param name="term">The term to search for</param>
        /// <returns>The results</returns>
        public async Task<List<IMusicBase>> Search(string term)
        {
            var results = new List<IMusicBase>();         
            var localArtists = await localSource.SearchArtists(term);
            var torchArtists = await torchSource.SearchArtists(term);

            results.AddRange(localArtists);
            results.AddRange(torchArtists.Where(a => localArtists.All(a2 => a.Name != a2.Name)));

            var localAlbums = await localSource.SearchAlbums(term);
            var torchAlbums = await torchSource.SearchAlbums(term);

            results.AddRange(localAlbums);
            results.AddRange(torchAlbums.Where(a => localAlbums.All(a2 => (!(a.Name == a2.Name && a.Artist == a2.Artist)))));

            var localTracks = await localSource.SearchTracks(term);
            var torchTracks = await torchSource.SearchTracks(term);

            results.AddRange(localTracks);
            results.AddRange(torchTracks.Where(a => localTracks.All(a2 => (!(a.Name == a2.Name && a.Artist == a2.Artist && a.Album == a2.Album)))));
            
            return results;
        }

        /// <summary>
        /// Loads the specified artist. If the database
        /// doesn't have the specified id it gets it from torch
        /// and adds it to the database
        /// </summary>
        /// <param name="id">The id of the artist</param>
        /// <returns>The located artist</returns>
        public async Task<Artist> GetArtist(string id)
        {
            var artist = await localSource.GetArtist(id);

            if (artist != null) return artist;

            artist = await torchSource.GetArtist(id);
            await localSource.InsertArtist(artist);

            return artist;
        }

        /// <summary>
        /// Updates an existing artist with additional information from torch
        /// </summary>
        /// <param name="artist">The artist to update</param>
        public async Task UpdateArtist(Artist artist)
        {
            Artist result;
            if (string.IsNullOrEmpty(artist.TorchId))
                result = (await torchSource.SearchArtists(artist.Name)).FirstOrDefault();
            else result = await torchSource.GetArtist(artist.TorchId);

            MergeHelper.MergeTorchArtist(artist, result);
            await localSource.InsertArtist(artist);
        }

        /// <summary>
        /// Gets the album with the specified ID. If it can't be found
        /// locally, it is fetched from torch
        /// </summary>
        /// <param name="id">The id of the album</param>
        /// <returns>The album</returns>
        public async Task<Album> GetAlbum(string id)
        {
            var album = await localSource.GetAlbum(id);

            if (album != null) return album;

            album = await torchSource.GetAlbum(id);
            await localSource.InsertAlbum(album);

            return album;
        }

        /// <summary>
        /// Updates an album with additional information from torch
        /// </summary>
        /// <param name="album">The album to update</param>
        public async Task UpdateAlbum(Album album)
        {
            Album result;
            if (string.IsNullOrEmpty(album.TorchId))
                result = (await torchSource.SearchAlbums(album.Name, album.Artist)).FirstOrDefault();
            else 
                result = await torchSource.GetAlbum(album.TorchId);

            MergeHelper.MergeTorchAlbum(album, result);
            await localSource.InsertAlbum(album);
        }

        /// <summary>
        /// Gets the track with the specified id. If it can't 
        /// be found locally, the track is fetched from
        /// torch
        /// </summary>
        /// <param name="id">The id of the track</param>
        /// <returns>The track</returns>
        public async Task<Track> GetTrack(string id)
        {
            var track = await localSource.GetTrack(id);

            if (track != null) return track;

            track = await torchSource.GetTrack(id);
            await localSource.InsertTrack(track);

            return track;
        }

        /// <summary>
        /// Updates a track with information from torch
        /// </summary>
        /// <param name="track">The track to update</param>
        public async Task UpdateTrack(Track track)
        {
            Track result;
            if (string.IsNullOrEmpty(track.TorchId))
                result = (await torchSource.SearchTracks(track.Name, track.Artist)).FirstOrDefault();
            else result = await torchSource.GetTrack(track.TorchId);

            MergeHelper.MergeTorchTrack(track, result);
            await localSource.InsertTrack(track);
        }
    }
}
