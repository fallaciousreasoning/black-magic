﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using TorchMusic;

namespace MusicBase
{
    [Table("Artists")]
    public class Artist : IMusicBase, IArtwork
    {
        [PrimaryKey]
        public string Id { get; set; }

        [Indexed]
        public string Name { get; set; }

        public string TorchId { get; set; }

        [Ignore]
        public List<Album> Albums { get; set; }

        public string ServerTag { get; set; }

        public string ImageTiny
        {
            get { return ImageHelper.GetImageUrl(ServerTag, ImageSize.Tiny); }
        }

        public string ImageSmall
        {
            get { return ImageHelper.GetImageUrl(ServerTag, ImageSize.Small); }
        }

        public string ImageMedium
        {
            get { return ImageHelper.GetImageUrl(ServerTag, ImageSize.Medium); }
        }

        public string ImageLarge
        {
            get { return ImageHelper.GetImageUrl(ServerTag, ImageSize.Large); }
        }

        public string ImageHuge
        {
            get { return ImageHelper.GetImageUrl(ServerTag, ImageSize.Huge); }
        }

        public Artist()
        {
            Albums = new List<Album>();
        }
    }
}
