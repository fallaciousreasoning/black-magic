﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using TorchMusic;
using YoutubeExtractor;

namespace MusicBase
{
    [Table("Tracks")]
    public class Track : IMusicBase
    {
        [PrimaryKey]
        public string Id { get; set; }

        /// <summary>
        /// The Id of the track on torch
        /// </summary>
        public string TorchId { get; set; }

        /// <summary>
        /// The name of the track
        /// </summary>
        [Indexed]
        public string Name { get; set; }

        /// <summary>
        /// The name of the artist
        /// </summary>
        [Indexed]
        public string Artist { get; set; }

        /// <summary>
        /// The name of the album
        /// </summary>
        [Indexed]
        public string Album { get; set; }

        /// <summary>
        /// The length of the track
        /// </summary>
        public int Length { get; internal set; }

        /// <summary>
        /// The number the track occurs in the album
        /// </summary>
        public int TrackNumber { get; internal set; }

        /// <summary>
        /// The Url for the video on youtube
        /// </summary>
        public string VideoUrl { get; set; }

        /// <summary>
        /// The path the the track. This is null if the track is online only
        /// </summary>
        public string FilePath { get; set; }

        public Track()
        {

        }

        public async Task<string> GetPath()
        {
            if (FilePath != null) return FilePath;

            if (string.IsNullOrEmpty(VideoUrl))
                VideoUrl = await (new TorchHelper()).GetYoutubeUrl(TorchId);

            var url = string.Format("http://www.youtube.com/watch?v={0}/", VideoUrl);

            var videoInfos = await DownloadUrlResolver.GetDownloadUrlsAsync(url);

            var info = (from videoInfo in videoInfos
                        where videoInfo.Resolution == 0
                        select videoInfo).FirstOrDefault();

            return info == null ? "" : info.DownloadUrl;
        }
    }
}
