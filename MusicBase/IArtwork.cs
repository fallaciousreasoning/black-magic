﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace MusicBase
{
    public interface IArtwork
    {
        string ServerTag { get; set; }

        [Ignore]
        string ImageTiny { get; }

        [Ignore]
        string ImageSmall { get; }

        [Ignore]
        string ImageMedium { get; }

        [Ignore]
        string ImageLarge { get; }

        [Ignore]
        string ImageHuge { get; }
    }
}
