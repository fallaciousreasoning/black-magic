﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicBase
{
    public interface IMusicBase
    {
        /// <summary>
        /// The name of the music object
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// The Id of the object on torch
        /// </summary>
        string TorchId { get; set; }
    }
}
