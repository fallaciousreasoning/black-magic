﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace MusicBase.MusicSources
{
    public class DatabaseMusicSource : IMusicSource
    {
        private const string DATABASE_NAME = "music.db";

        private readonly SQLiteAsyncConnection connection;

        public DatabaseMusicSource()
        {
            connection = new SQLiteAsyncConnection(DATABASE_NAME);
            connection.CreateTableAsync<Artist>();
            connection.CreateTableAsync<Album>();
            connection.CreateTableAsync<Track>();
            connection.CreateTableAsync<IdManager>();
        }

        #region Querying

        public async Task<bool> ArtistExists(Artist artist)
        {
            return (await connection.FindAsync<Artist>(artist.Id)) != null;
        }

        public async Task<bool> ArtistExists(string artistName)
        {
            return await connection.Table<Artist>().Where(a => a.Name == artistName).CountAsync() > 0;
        }

        public async Task<bool> AlbumExists(Album album)
        {
            return (await connection.FindAsync<Album>(album.Id)) != null;
        }

        public async Task<bool> AlbumExists(string artistName, string albumName)
        {
            return (await SearchAlbums(albumName, artistName)).Count != 0;
        }

        public async Task<bool> TrackExists(Track track)
        {
            return (await connection.FindAsync<Track>(track.Id)) != null;
        }

        public async Task<bool> TrackExists(string artistName, string albumName, string trackName)
        {
            return (await SearchTracks(trackName, artistName, albumName)).Count != 0;
        }

        public async Task<Artist> GetArtist(string id)
        {
            var artist = await connection.FindAsync<Artist>(id);
            artist.Albums = await SearchAlbums("", artist.Name);
            return artist;
        }

        public async Task<Album> GetAlbum(string id)
        {
            var album = await connection.FindAsync<Album>(id);
            album.Tracks = await SearchTracks("", album.Artist, album.Name);
            return album;
        }

        public async Task<Track> GetTrack(string id)
        {
            return await connection.FindAsync<Track>(id);
        }

        public async Task<List<Artist>> SearchArtists(string search)
        {
            search = search.ToLower();
            var artists = await connection.Table<Artist>().Where(a => a.Name.ToLower().Contains(search)).ToListAsync();

            for (int i = 0; i < artists.Count; ++i)
                artists[i] = await GetArtist(artists[i].Id);

            return artists;
        }

        public async Task<List<Album>> SearchAlbums(string searchTerm, string artistName = null)
        {
            searchTerm = searchTerm.ToLower();
            var albums =
                await
                    connection.Table<Album>()
                        .Where(a => a.Name.ToLower().Contains(searchTerm) && (artistName == null || a.Artist == null || a.Artist == artistName))
                        .ToListAsync();

            for (int i = 0; i < albums.Count; i++)
                albums[i] = await GetAlbum(albums[i].Id);

            return albums;
        }

        public async Task<List<Track>> SearchTracks(string search, string artistName = null, string albumName = null)
        {
            search = search.ToLower();
            var tracks =
                await
                    connection.Table<Track>()
                        .Where(
                            t =>
                                t.Name.ToLower().Contains(search) && (albumName == null || t.Album == null || t.Album == albumName) &&
                                (artistName == null || t.Artist == null || artistName == t.Artist)).ToListAsync();
            return tracks;
        }

        #endregion

        #region Insert

        public async Task InsertArtist(Artist artist)
        {
            if (!await ArtistExists(artist))
            {
                artist.Id = await IdManager.GetNextId<Artist>(connection);
                await connection.InsertAsync(artist);
            }
            else await connection.UpdateAsync(artist);

            foreach (var album in artist.Albums)
                await InsertAlbum(album);
        }

        public async Task InsertAlbum(Album album)
        {
            if (!await AlbumExists(album))
            {
                album.Id = await IdManager.GetNextId<Album>(connection);
                await connection.InsertAsync(album);
            }
            else await connection.UpdateAsync(album);

            foreach (var track in album.Tracks)
                await InsertTrack(track);
        }

        public async Task InsertTrack(Track track)
        {
            if (!await TrackExists(track))
            {
                track.Id = await IdManager.GetNextId<Track>(connection);
                await connection.InsertAsync(track);
            }
            else await connection.UpdateAsync(track);
        }

        #endregion
    }
}
