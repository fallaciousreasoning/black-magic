﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicBase.MusicSources
{
    public interface IMusicSource
    {
        Task<Artist> GetArtist(string id);
        Task<Album> GetAlbum(string id);
        Task<Track> GetTrack(string id);

        Task<List<Artist>> SearchArtists(string artistName);
        Task<List<Album>> SearchAlbums(string albumName, string artistName = null);
        Task<List<Track>> SearchTracks(string trackName, string artistName = null, string albumName = null);
    }
}
