﻿using MusicBase.MusicSources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TorchMusic;

namespace MusicBase.Torch
{
    public class TorchMusicSource : IMusicSource
    {
        private TorchHelper helper;

        public TorchMusicSource()
        {
            helper = new TorchHelper();
        }

        #region Fetching

        public async Task<Artist> GetArtist(string id)
        {
            var artist = await helper.GetArtistAsync(id);
            return CreateFromTorch(artist);
        }

        public async Task<Album> GetAlbum(string id)
        {
            var album = await helper.GetAlbumAsync(id);
            return CreateFromTorch(album);
        }

        public async Task<Track> GetTrack(string id)
        {
            var track = await helper.GetTrack(id);
            return CreateFromTorch(track);
        }

        #endregion

        public async Task<List<Artist>> SearchArtists(string artistName)
        {
            var torchResults = await helper.Search(artistName, searchFor: SearchFor.Artists);
            return torchResults.Select(result => CreateFromTorch(result as TorchArtist)).ToList();
        }

        public async Task<List<Album>> SearchAlbums(string albumName, string artistName = null)
        {
            var torchResults = await helper.Search(artistName, searchFor: SearchFor.Albums);
            return torchResults.Select(result => CreateFromTorch(result as TorchAlbum)).ToList();
        }

        public async Task<List<Track>> SearchTracks(string trackName, string artistName = null, string albumName = null)
        {
            var torchResults = await helper.Search(artistName, searchFor: SearchFor.Artists);
            return torchResults.Select(result => CreateFromTorch(result as TorchTrack)).ToList();
        }


        #region Conversion

        public static Artist CreateFromTorch(TorchArtist tartist)
        {
            var artist = new Artist() {Name = tartist.Name, TorchId = tartist.Id};

            AddArtworkFromTorch(artist, tartist.ImageUrl);

            artist.Albums = (from album in tartist.Albums select CreateFromTorch(album)).ToList();

            return artist;
        }

        public static Album CreateFromTorch(TorchAlbum talbum)
        {
            var album = new Album() {Name = talbum.Name, TorchId = talbum.Id};
            album.Artist = talbum.Artist;
            album.ReleaseDate = talbum.ReleaseDate;

            AddArtworkFromTorch(album, talbum.ImageUrl);

            var tracks = new List<Track>();
            foreach (var track in talbum.Tracks)
                tracks.Add(CreateFromTorch(track));

            album.Tracks = tracks;

            return album;
        }

        public static Track CreateFromTorch(TorchTrack ttrack)
        {
            var track = new Track() {Name = ttrack.Name, TorchId = ttrack.Id};
            track.Artist = ttrack.Artist;
            track.Album = ttrack.Album;
            track.Length = ttrack.Length;
            track.TrackNumber = ttrack.TrackNumber;

            track.VideoUrl = ttrack.Url;

            return track;
        }

        private static void AddArtworkFromTorch(IArtwork to, string serverTag)
        {
            to.ServerTag = serverTag;
        }

        #endregion
    }
}
