﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicBase.MusicSources
{
    internal class MergeHelper
    {
        internal static void MergeTorchArtist(Artist original, Artist torchArtist)
        {
            if (torchArtist == null) return;
            
            original.TorchId = torchArtist.TorchId;

            MergeArtwork(original, torchArtist);

            foreach (var album in torchArtist.Albums)
            {
                var existing = original.Albums.FirstOrDefault(a => a.Name == album.Name);
                if (existing == null)
                    original.Albums.Add(album);
                else MergeTorchAlbum(existing, album);
            }
        }

        internal static void MergeTorchAlbum(Album original, Album torchAlbum)
        {
            if (torchAlbum == null) return;

            original.TorchId = torchAlbum.TorchId;
            if (string.IsNullOrEmpty(original.ReleaseDate))
                original.ReleaseDate = torchAlbum.ReleaseDate;

            MergeArtwork(original, torchAlbum);

            foreach (var track in torchAlbum.Tracks)
            {
                var existing = original.Tracks.FirstOrDefault(t => t.Name == track.Name);
                if (existing == null)
                    original.Tracks.Add(track);
                else MergeTorchTrack(existing, track);
            }
        }

        private static void MergeArtwork(IArtwork original, IArtwork torch)
        {
            original.ServerTag = torch.ServerTag;
        }

        internal static void MergeTorchTrack(Track original, Track torchTrack)
        {
            if (torchTrack == null) return;

            original.TorchId = torchTrack.TorchId;
            if (original.TrackNumber == 0)
                original.TrackNumber = torchTrack.TrackNumber;

            original.VideoUrl = torchTrack.VideoUrl;
        }
    }
}
