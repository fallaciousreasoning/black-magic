﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace MusicBase.MusicSources
{
    [Table("IDENTIFIERS")]
    internal class IdManager
    {
        [PrimaryKey]
        public string TableName { get; set; }
        
        public string LastIdentifier { get; set; }

        public IdManager(Type type)
            :this()
        {
            this.TableName = type.FullName;
        }

        public IdManager()
        {
            LastIdentifier = "0";
        }

        public static async Task<string> GetNextId<T>(SQLiteAsyncConnection connection)
            where T : class, new()
        {
            var c = await connection.FindAsync<IdManager>(typeof (T).FullName);

            if (c == null)
            {
                c = new IdManager(typeof(T));
                await connection.InsertAsync(c);

                return c.LastIdentifier;
            }

            c.LastIdentifier = GetNextId(c.LastIdentifier);
            await connection.UpdateAsync(c);

            return c.LastIdentifier;
        }

        private static string GetNextId(string from)
        {
            var number = Convert.ToInt64(from);
            number ++;

            return number.ToString();
        }
    }
}
