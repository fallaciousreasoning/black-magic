﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using TorchMusic;

namespace MusicBase
{
    [Table("Albums")]
    public class Album : IMusicBase, IArtwork
    {
        [PrimaryKey]
        public string Id { get; set; }

        [Indexed]
        public string Name { get; set; }

        public string TorchId { get; set; }
        public string ReleaseDate { get; set; }

        [Ignore]
        public List<Track> Tracks { get; set; }

        [Indexed]
        public string Artist { get; set; }

        public Album()
        {
            Tracks = new List<Track>();
        }

        public string ServerTag { get; set; }

        public string ImageTiny
        {
            get { return ImageHelper.GetImageUrl(ServerTag, ImageSize.Tiny); }
        }

        public string ImageSmall
        {
            get { return ImageHelper.GetImageUrl(ServerTag, ImageSize.Small); }
        }

        public string ImageMedium
        {
            get { return ImageHelper.GetImageUrl(ServerTag, ImageSize.Medium); }
        }

        public string ImageLarge
        {
            get { return ImageHelper.GetImageUrl(ServerTag, ImageSize.Large); }
        }

        public string ImageHuge
        {
            get { return ImageHelper.GetImageUrl(ServerTag, ImageSize.Huge); }
        }
    }
}
