﻿using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556
using BlackMagic.ViewModels;
using MusicBase;

namespace BlackMagic.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ArtistPage : Page
    {
        public ArtistPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            string id = null;

            if (e.Parameter is string)
                id = e.Parameter as string;

            var artist = e.Parameter is Artist ? e.Parameter as Artist : await App.Music.GetArtist(id);
            if (artist == null) throw new ArgumentException("When navigating to the ArtistPage you must provide a valid torch artist or the id of a valid torch artist!", "e.Parameter");
            
            (DataContext as ArtistViewModel).Artist = artist;
        }
    }
}
