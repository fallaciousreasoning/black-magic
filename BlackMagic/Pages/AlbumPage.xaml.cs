﻿using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556
using BlackMagic.ViewModels;
using MusicBase;

namespace BlackMagic.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AlbumPage : Page
    {
        public AlbumPage()
        {
            this.InitializeComponent();
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            string id = null;

            if (e.Parameter is string)
                id = e.Parameter as string;

            var album = e.Parameter is Album ? e.Parameter as Album : await App.Music.GetAlbum(id);
            if (album == null) throw new ArgumentException("When navigating to the AlbumPage you must provide a valid torch album or the id of a valid torch album!", "e.Parameter");

            (DataContext as AlbumViewModel).Album = album;
        }
    }
}
