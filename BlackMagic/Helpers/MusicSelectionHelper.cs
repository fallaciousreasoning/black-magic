﻿using Windows.Foundation.Collections;
using Windows.Media.Playback;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using BlackMagic.Pages;
using Playlist;
using MusicBase;

namespace BlackMagic.Helpers
{
    public static class MusicSelectionHelper
    {
        /// <summary>
        /// Handles selecting a track
        /// </summary>
        /// <param name="result">The result pointing to a track</param>
        public static async void SelectTrack(MusicBase.Track result)
        {
            if (result == null) return;

            var url = await result.GetPath();
            App.NowPlaying.Add(new Playlist.Track() { Album = result.Album, Artist = result.Artist, Name = result.Name, Uri = url }, 0);
            await PlaylistHelper.SaveNowPlaying(App.NowPlaying);
            BackgroundMediaPlayer.SendMessageToBackground(new ValueSet() { { Constants.UPDATE_PLAYLIST, "" } });

        }

        /// <summary>
        /// Handles selecting an artist
        /// </summary>
        /// <param name="result">The result pointing to an artist</param>
        public static void SelectArtist(Artist result)
        {
            var frame = (Window.Current.Content as Frame);
            frame.Navigate(typeof(ArtistPage), result);
        }

        /// <summary>
        /// Handles selecting an album
        /// </summary>
        /// <param name="result">The result pointing to an album</param>
        public  static void SelectAlbum(Album result)
        {
            var frame = (Window.Current.Content as Frame);
            frame.Navigate(typeof(AlbumPage), result);
        }
    }
}
