﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using MusicBase;

namespace BlackMagic.TemplateSelectors
{
    public class SearchResultTemplateSelector : DataTemplateSelector
    {
        public DataTemplate ArtistTemplate { get; set; }
        public DataTemplate AlbumTemplate { get; set; }
        public DataTemplate TrackTemplate { get; set; }

        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container)
        {
            if (item is Artist)
                return ArtistTemplate;

            if (item is Album)
                return AlbumTemplate;

            if (item is Track)
                return TrackTemplate;

            return null;
        }
    }
}
