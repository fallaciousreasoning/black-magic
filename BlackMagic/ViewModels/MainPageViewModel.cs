﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackMagic.Pages;

namespace BlackMagic.ViewModels
{
    public class MainPageViewModel
    {
        public SearchViewModel SearchViewModel { get; set; }
        public RecentlyPlayedViewModel RecentlyPlayedViewModel { get; set; }

        public MainPageViewModel()
        {
            SearchViewModel = new SearchViewModel();
            RecentlyPlayedViewModel = new RecentlyPlayedViewModel();
        }
    }
}
