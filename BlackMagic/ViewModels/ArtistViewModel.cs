﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using BlackMagic.Annotations;
using BlackMagic.Helpers;
using MusicBase;

using Track = MusicBase.Track;

namespace BlackMagic.ViewModels
{
    public class ArtistViewModel : INotifyPropertyChanged
    {
        private Album selectedAlbum;
        private Track selectedTrack;

        private Artist artist;

        /// <summary>
        /// The tracks by this artist
        /// </summary>
        public ObservableCollection<Track> Tracks { get; set; } 
        
        /// <summary>
        /// The albums by this artist
        /// </summary>
        public ObservableCollection<Album> Albums { get; set; }

        /// <summary>
        /// The artist this view model is based around
        /// </summary>
        public Artist Artist
        {
            get { return artist; }
            set
            {
                if (artist == value) return;

                artist = value;

                RefreshAlbumsAndTracks();
                UpdateArtist();

                OnPropertyChanged();
            }
        }

        /// <summary>
        /// The currently selected album
        /// </summary>
        public Album SelectedAlbum
        {
            get { return selectedAlbum; }
            set
            {
                if (selectedAlbum == value) return;
                
                selectedAlbum = value;
                OnPropertyChanged();
                
                MusicSelectionHelper.SelectAlbum(selectedAlbum);
            }
        }

        /// <summary>
        /// The currently selected track
        /// </summary>
        public Track SelectedTrack
        {
            get { return selectedTrack; }
            set
            {
                if (selectedTrack == value) return;
                
                selectedTrack = value;
                OnPropertyChanged();


                MusicSelectionHelper.SelectTrack(selectedTrack);
            }
        }

        /// <summary>
        /// Default constructor, initialize collections
        /// </summary>
        public ArtistViewModel()
        {
            Albums = new ObservableCollection<Album>();
            Tracks = new ObservableCollection<Track>();
        }

        /// <summary>
        /// Updates the currently selected artist
        /// </summary>
        private async void UpdateArtist()
        {
            await App.Music.UpdateArtist(artist);

            RefreshAlbumsAndTracks();

            for (int i = 0; i < Artist.Albums.Count; ++i)            
                await App.Music.UpdateAlbum(Artist.Albums[i]);

            RefreshAlbumsAndTracks();
        }

        private void RefreshAlbumsAndTracks()
        {
            Albums.Clear();
            Tracks.Clear();

            foreach (var album in artist.Albums)
            {
                Albums.Add(album);
                foreach (var track in album.Tracks)
                    Tracks.Add(track);
            }
        }

        #region INotifyPropertyChanged Implementation

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}
