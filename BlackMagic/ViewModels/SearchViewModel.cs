﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using BlackMagic.Helpers;
using MusicBase;

namespace BlackMagic.ViewModels
{
    public class SearchViewModel : INotifyPropertyChanged
    {
        #region Properties and Fields

        #region Fields

        private string nextQuery;
        private bool searching;

        private string query;
        private IMusicBase selectedItem;

        #endregion


        #region Properties

        /// <summary>
        /// The term being searched for
        /// </summary>
        public string Query
        {
            get { return query; }
            set
            {
                if (query == value) return;
                query = value;
                OnPropertyChanged();

                GetResults();
            }
        }

        /// <summary>
        /// The currently selected search result
        /// </summary>
        public IMusicBase SelectedItem
        {
            get { return selectedItem; }
            set
            {
                if (selectedItem == value) return;

                selectedItem = value;

                UpdateSelected();
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// A list of the results for the query
        /// </summary>
        public ObservableCollection<IMusicBase> Results { get; set; }

        #endregion


        #endregion

        #region Methods

        #region Search

        private async void GetResults()
        {
            if (!string.IsNullOrEmpty(nextQuery))
            {
                nextQuery = query;
                return;
            }

            nextQuery = query;

            var results = await App.Music.Search(query);

            Results.Clear();

            foreach (var result in results)
                Results.Add(result);

            if (!string.IsNullOrEmpty(nextQuery))
            {
                query = nextQuery;
                nextQuery = string.Empty;
                GetResults();
            }
        }

        #endregion

        #region Selecting

        private void UpdateSelected()
        {
            if (selectedItem == null) return;
            
            if (selectedItem is Track)
                    MusicSelectionHelper.SelectTrack(selectedItem as Track);
            if (selectedItem is Artist)
                    MusicSelectionHelper.SelectArtist(selectedItem as Artist);
            if (selectedItem is Album)
                    MusicSelectionHelper.SelectAlbum(selectedItem as Album);
        }

        #endregion

        #endregion


        #region Constructor

        public SearchViewModel()
        {
            Results = new ObservableCollection<IMusicBase>();
        }

        #endregion


        #region INotifyPropertyChanged Implementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}
