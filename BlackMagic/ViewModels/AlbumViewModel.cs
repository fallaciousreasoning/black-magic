﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using BlackMagic.Annotations;
using BlackMagic.Helpers;
using MusicBase;

using Track = MusicBase.Track;

namespace BlackMagic.ViewModels
{
    public class AlbumViewModel : INotifyPropertyChanged
    {
        private Album album;
        private Track selectedTrack;

        public ObservableCollection<Track> Tracks { get; set; }

        public Track SelectedTrack
        {
            get { return selectedTrack; }
            set
            {
                if (selectedTrack == value) return;

                selectedTrack = value;
                OnPropertyChanged();


                MusicSelectionHelper.SelectTrack(selectedTrack);
            }
        }

        public Album Album
        {
            get { return album; }
            set
            {
                if (album == value) return;

                album = value;
                RefreshTracks();
                UpdateAlbum();
                OnPropertyChanged();
            }
        }

        public AlbumViewModel()
        {
            Tracks = new ObservableCollection<Track>();
        }

        private async void UpdateAlbum()
        {
            await App.Music.UpdateAlbum(album);
            RefreshTracks();
        }

        private void RefreshTracks()
        {
            Tracks.Clear();

            foreach (var track in album.Tracks)
                Tracks.Add(track);
        }

        #region INotifyPropertyChanged Implementation

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}
