﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using MusicBase;

namespace BlackMagic.ViewModels
{
    public class RecentlyPlayedViewModel : INotifyPropertyChanged
    {
        public NowPlayingViewModel NowPlayingViewModel { get; set; }

        public RecentlyPlayedViewModel()
        {
            NowPlayingViewModel = new NowPlayingViewModel();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
