﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Windows.Storage;
using Windows.UI.Xaml;
using Newtonsoft.Json;

namespace Playlist
{
    public static class PlaylistHelper
    {
        private const string NOW_PLAYING_NAME = "nowplaying.json";
        private const string PLAYLIST_SETTINGS_NAME = "playlistsettings.dat";

        public static async Task SaveNowPlaying(Playlist playlist)
        {
            var text = await Task.Run(() => JsonConvert.SerializeObject(playlist));

            var file = await ApplicationData.Current.LocalFolder.CreateFileAsync(NOW_PLAYING_NAME, CreationCollisionOption.ReplaceExisting);
            await FileIO.WriteTextAsync(file, text);
        }

        public static async Task<Playlist> LoadNowPlaying()
        {
            try
            {
                var file = await ApplicationData.Current.LocalFolder.GetFileAsync(NOW_PLAYING_NAME);
                var text = await FileIO.ReadTextAsync(file);
                var playlist = await Task.Run(() => JsonConvert.DeserializeObject<Playlist>(text));
                return playlist;
            }
            catch
            {
                Debug.WriteLine("Failed to open now playing file. File may not have been created");
                return null;
            }
        }

        public static async Task LoadNowPlaying(this PlaylistManager playlistManager)
        {
            playlistManager.Playlist = await LoadNowPlaying();
        }

        public static async Task SaveSettings(this PlaylistManager playlistManager)
        {
            var writer = new BinaryWriter(await (await ApplicationData.Current.LocalFolder.CreateFileAsync(PLAYLIST_SETTINGS_NAME, CreationCollisionOption.ReplaceExisting)).OpenStreamForWriteAsync());
            writer.Write(playlistManager.Shuffled);
            writer.Write((int) playlistManager.RepeatMode);
            writer.Write(playlistManager.PlaylistPos);

            writer.Dispose();
        }

        public static async Task LoadSettings(this PlaylistManager playlistManager)
        {
            try
            {
                var reader = new BinaryReader(await (await ApplicationData.Current.LocalFolder.GetFileAsync(PLAYLIST_SETTINGS_NAME)).OpenStreamForReadAsync());
                playlistManager.Shuffled = reader.ReadBoolean();
                playlistManager.RepeatMode = (RepeatMode)reader.ReadInt32();
                playlistManager.PlaylistPos = reader.ReadInt32();

                reader.Dispose();
            }
            catch
            {
                Debug.WriteLine("Failed to open now playlist settings file. File may not have been created");
            }
        }
    }
}
