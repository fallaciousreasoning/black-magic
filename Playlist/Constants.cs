﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Playlist
{
    public class Constants
    {
        /// <summary>
        /// Indicates the current playlist should be reloaded from the disk
        /// </summary>
        public const string UPDATE_PLAYLIST = "updateplaylist";
        
        /// <summary>
        /// Indicates the current settings should be reloaded from the disk
        /// </summary>
        public const string UPDATE_SETTINGS = "updatesettings";

        /// <summary>
        /// Indicates a message containing the track number of the new tracks
        /// </summary>
        public const string SET_CURRENT_TRACK = "setcurrenttrack";

    }
}
