﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Playlist
{
    [Flags]
    public enum RepeatMode
    {
        None = 1,
        All = 2,
        Once = 4,
    }

    public delegate void TrackChanged(Track newTrack);

    public class PlaylistManager
    {
        private static readonly Random random = new Random();

        private bool shuffled;

        /// <summary>
        /// Called when the current track is changed
        /// </summary>
        public event TrackChanged TrackChanged;

        /// <summary>
        /// The current track
        /// </summary>
        public Track CurrentTrack
        {
            get { return Playlist.Tracks[PlaylistPos]; }
        }

        /// <summary>
        /// Whether the playlist is shuffled or not
        /// </summary>
        public bool Shuffled
        {
            get { return shuffled; }
            set
            {
                if (shuffled == value) return;

                if (value)
                    Shuffle();
                else Order();

                shuffled = value;
            }
        }

        /// <summary>
        /// The repeat mode of the playlist
        /// </summary>
        public RepeatMode RepeatMode { get; set; }

        /// <summary>
        /// The current playlist
        /// </summary>
        public Playlist Playlist { get; set; }

        //The current position in the playlist
        internal int PlaylistPos { get; set; }

        public PlaylistManager()
        {
            Playlist = new Playlist();
        }

        /// <summary>
        /// Puts the tracks into a random order
        /// </summary>
        private void Shuffle()
        {
            var randomised = new List<Track>(Playlist.Tracks.Count);
            while (Playlist.Tracks.Count > 0)
            {
                var pick = random.Next(Playlist.Tracks.Count);
                randomised.Add(Playlist.Tracks[pick]);
                Playlist.Tracks.RemoveAt(pick);
            }

            Playlist.Tracks = randomised;
        }

        /// <summary>
        /// Orders the tracks by their track number
        /// </summary>
        private void Order()
        {
            Playlist.Tracks = Playlist.Tracks.OrderBy(t => t.TrackNumber).ToList();
        }

        /// <summary>
        /// Moves to the next track, according to the repeat and shuffle modes
        /// </summary>
        /// <param name="forceMove">If force move is on then this method will take you to the next song, regardless
        /// of what your repeat mode is
        /// </param>
        /// <returns>The new current track</returns>
        public string MoveNext(bool forceMove=false)
        {
            //If we're not just repeating one song, or the user has forced the move
            if (RepeatMode != RepeatMode.Once || forceMove)
                PlaylistPos++;

            //If we're repeating everything, or the user has forced the move, wrap
            if (RepeatMode == RepeatMode.All || forceMove)
                PlaylistPos %= Playlist.Tracks.Count;

            OnTrackChanged();

            return CurrentTrack.Uri; //await CurrentTrack.GetStreamPath();
        }

        /// <summary>
        /// Moves to the previous track
        /// </summary>
        /// <returns>Gets the path of the stream for this track</returns>
        public string MovePrevious()
        {
            PlaylistPos--;
            if (PlaylistPos < 0)
                PlaylistPos = Playlist.Tracks.Count - 1;

            OnTrackChanged();

            return CurrentTrack.Uri;
        }

        /// <summary>
        /// Changes the currently playing track to the track with the specified track number
        /// </summary>
        /// <param name="trackNo">The number of the track to change to. This is usually it's position in the unshuffled playlist</param>
        /// <returns>A Uri pointing to the new track</returns>
        public string JumpTo(int trackNo)
        {
            var number = -1;
            for (var i = 0; i < Playlist.Tracks.Count; ++i)
                if (Playlist.Tracks[i].TrackNumber == trackNo)
                {
                    number = i;
                    break;
                }

            if (number == -1) return "";

            PlaylistPos = number;
            return CurrentTrack.Uri;
        }

        /// <summary>
        /// Called when the track is changed by the MoveNext
        /// or MovePrevious methods
        /// </summary>
        private void OnTrackChanged()
        {
            if (TrackChanged != null)
                TrackChanged(CurrentTrack);
        }
    }
}
