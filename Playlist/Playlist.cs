﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Playlist
{
    public class Playlist
    {
        /// <summary>
        /// The tracks in this playlist
        /// </summary>
        public List<Track> Tracks { get; set; }

        public Playlist()
        {
            Tracks = new List<Track>();
        }

        /// <summary>
        /// Adds a track at the specified index
        /// </summary>
        /// <param name="track">The track to add</param>
        /// <param name="index">The index to add it at</param>
        public void Add(Track track, int index)
        {
            track.TrackNumber = index++;

            if (index < Tracks.Count)
                Tracks.Insert(index, track);
            else Tracks.Add(track);

            while (index < Tracks.Count)
                Tracks[index++].TrackNumber++;
        }

        /// <summary>
        /// Removes the track at the index
        /// </summary>
        /// <param name="index">The index</param>
        public void Remove(int index)
        {
            Tracks.RemoveAt(index);
            while (index < Tracks.Count)
                Tracks[index++].TrackNumber--;
        }
    }
}
